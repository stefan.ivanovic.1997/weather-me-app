import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WORD_MATCH_REGEX} from '../../../core/constants';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/user.model';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  hide = true;
  error;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private title: Title,
    private authService: AuthService,
    private loader: NgxSpinnerService,
  ) {
    this.title.setTitle('WeatherMe | Register');
  }

  ngOnInit(): void {
    const {required, min, pattern} = {
      required: Validators.required,
      min: Validators.minLength(3),
      pattern: Validators.pattern(WORD_MATCH_REGEX)
    };
    this.registerForm = this.formBuilder.group({
      lastName: ['', [required, min, pattern]],
      name: ['', [required, min, pattern]],
      username: ['', [required, min]],
      password: ['', [required, Validators.minLength(6)]]
    });
  }

  public submit(): void {
    this.error = null;
    if (!this.registerForm.valid) {
      this.lastName.markAsDirty();
      this.name.markAsDirty();
      this.password.markAsDirty();
      this.username.markAsDirty();
      return;
    }
    const user: User = {...this.registerForm.value};
    this.loader.show();
    this.authService.register(user)
      .subscribe((res: User) => {
        this.loader.hide();
        this.router.navigateByUrl('auth/login');
      }, (err) => {
        this.error = err.error.message;
        this.loader.hide();
      });
  }


  get name(): any {
    return this.registerForm.get('name');
  }

  get lastName(): any {
    return this.registerForm.get('lastName');
  }

  get password(): any {
    return this.registerForm.get('password');
  }

  get username(): any {
    return this.registerForm.get('username');
  }
}
