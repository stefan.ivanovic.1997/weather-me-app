import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../models/user.model';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../services/auth.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public hide = true;
  public error = null;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private title: Title,
    private authService: AuthService,
    private loader: NgxSpinnerService,
  ) {
    this.title.setTitle('WeatherMe | Login');
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  submit(): void {
    if (!this.loginForm.valid) {
      this.username.markAsDirty();
      this.password.markAsDirty();
      return;
    }
    const user: User = {...this.loginForm.value};
    this.loader.show();
    this.authService.login(user)
      .subscribe((res: User) => {
        this.authService.saveUser(res);
        this.loader.hide();
        this.router.navigateByUrl('/weather');
      }, (err) => {
        this.error = err.error.message;
        this.loader.hide();
      });
  }

  get username(): any {
    return this.loginForm.get('username');
  }

  get password(): any {
    return this.loginForm.get('password');
  }

}
