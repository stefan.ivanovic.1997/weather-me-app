import {Injectable} from '@angular/core';
import {User} from '../models/user.model';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiService} from '../../shared/services/api.service';
import {EncryptService} from '../../shared/services/encrypt.service';
import {AUTH_LOCAL_KEY} from '../../core/constants';
import {take} from 'rxjs/operators';

@Injectable()
export class AuthService {

  private $user: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  user = this.$user.asObservable();

  constructor(private api: ApiService, private encrypt: EncryptService) {
  }

  getUser(): User {
    return this.encrypt.decryptData(localStorage.getItem(AUTH_LOCAL_KEY));
  }

  login(user: User): Observable<any> {
    return this.api.post('auth/login', user).pipe(take(1));
  }

  register(user: User): Observable<any> {
    return this.api.post('auth/register', user).pipe(take(1));
  }

  changeUser(fahrenheit: boolean): Observable<any> {
    return this.api.post('users/fahrenheit', {fahrenheit}).pipe(take(1));
  }

  saveUser(user: User): void {
    localStorage.setItem(AUTH_LOCAL_KEY, this.encrypt.encryptData(user));
    this.$user.next(user);
  }

  logout(): void {
    localStorage.removeItem(AUTH_LOCAL_KEY);
    this.$user.next(null);
  }

  reloadUser(): void {
    const user = this.encrypt.decryptData(localStorage.getItem(AUTH_LOCAL_KEY));
    if (user) {
      this.$user.next(user);
    }
  }

  private getUserFromDb(id: number): Observable<User> {
    return this.api.get(`users/${id}`);
  }

  reloadUserFromDb(id: number): void {
    this.getUserFromDb(id).pipe(take(1)).subscribe((user: User) => {
      user.accessToken = this.$user.getValue().accessToken;
      this.saveUser(user);
    });
  }
}
