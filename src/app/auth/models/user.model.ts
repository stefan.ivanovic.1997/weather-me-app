export class User {
  id?: string;
  name?: string;
  lastName?: string;
  accessToken?: string;
  username?: string;
  password?: string;
  fahrenheit?: boolean;
  favourites?: any;
}
