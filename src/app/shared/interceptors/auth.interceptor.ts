import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../../auth/services/auth.service';
import {switchMap} from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.auth.user
      .pipe(
        switchMap(user => {
          if (user && user.accessToken) {
            request = request.clone({
              setHeaders: {
                Authorization: `Bearer ${user.accessToken}`
              }
            });
          }
          return next.handle(request);
        }));
  }
}
