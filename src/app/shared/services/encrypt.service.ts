import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {ENCRYPTION_SECRET} from '../../core/constants';

@Injectable({
  providedIn: 'root'
})
export class EncryptService {

  constructor() {
  }

  encryptData(data): string {
    return CryptoJS.AES.encrypt(JSON.stringify(data), ENCRYPTION_SECRET).toString();
  }

  decryptData(data): any {
    try {
      const res = CryptoJS.AES.decrypt(data, ENCRYPTION_SECRET);
      return JSON.parse(res.toString(CryptoJS.enc.Utf8));
    } catch (e) {

    }
    return null;
  }
}
