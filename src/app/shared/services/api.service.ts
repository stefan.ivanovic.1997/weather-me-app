import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private endpoint = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  public get(route: string): Observable<any> {
    return this.http.get(`${this.endpoint}/${route}`);
  }

  public post(route: string, data: any): Observable<any> {
    return this.http.post(`${this.endpoint}/${route}`, data);
  }

  public delete(route: string, param: any): Observable<any> {
    return this.http.delete(`${this.endpoint}/${route}/${param}`);
  }
}
