export const WORD_MATCH_REGEX = /^[A-Z][a-zA-Z]+$/;
export const AUTH_LOCAL_KEY = 'Auth';
export const ENCRYPTION_SECRET = 'WeatherMe';
export const ENGLISH_LETTERS_REGEX = /^[A-Za-z][A-Za-z0-9]*\s-\s*[A-Za-z][A-Za-z0-9]*$/;
