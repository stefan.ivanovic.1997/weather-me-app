import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../../auth/services/auth.service';
import {Router} from '@angular/router';
import {FormControl} from '@angular/forms';
import {User} from '../../../auth/models/user.model';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {

  celsius = new FormControl(true);
  private user: User = null;
  private celsiusSub: Subscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private toaster: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.user = this.authService.getUser();
    this.celsius.setValue(this.user.fahrenheit);
    this.celsiusSub = this.celsius.valueChanges.subscribe(value => {
      this.authService.changeUser(value).subscribe(res => {
        this.user.fahrenheit = value;
        this.authService.saveUser(this.user);
      }, err => this.toaster.error('Error retrieving user'));
    });
  }

  ngOnDestroy(): void {
    this.celsiusSub.unsubscribe();
  }

  logout(): void {
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
  }

}
