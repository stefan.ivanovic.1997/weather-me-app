import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageNotFoundComponent} from './views/page-not-found/page-not-found.component';
import {SharedModule} from '../shared/shared.module';
import {AuthGuardService} from '../shared/services/guards/auth.guard.service';
import {RegisteredGuardService} from '../shared/services/guards/registered.guard.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from '../shared/interceptors/auth.interceptor';
import {AuthService} from '../auth/services/auth.service';


@NgModule({
  declarations: [
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [
    AuthGuardService,
    RegisteredGuardService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {
}
