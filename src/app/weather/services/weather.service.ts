import {Injectable} from '@angular/core';
import {ApiService} from '../../shared/services/api.service';
import {Observable} from 'rxjs';
import {Favourite, Geolocation} from '../models/favourite.model';
import {take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private apiService: ApiService) {
  }

  getInitialLocation(geolocation: Geolocation): Observable<any> {
    return this.apiService.get(`weather/geolocation?lat=${geolocation.lat}&long=${geolocation.long}`)
  }

  getLocations(value: string): Observable<any> {
    return this.apiService.get(`weather/locations?q=${value}`);
  }

  getWeather(obj: any): Observable<any> {
    return this.apiService.get(`weather/${obj.key}?fahrenheit=${obj.fahrenheit}`).pipe(take(1));
  }

  getFavourites(): Observable<Favourite[]> {
    return this.apiService.get(`weather/favourites`).pipe(take(1));
  }

  addFavourites(data: Favourite): Observable<Favourite> {
    return this.apiService.post(`weather/favourites`, data).pipe(take(1));
  }

  deleteFavourites(id: number): Observable<void> {
    return this.apiService.delete(`weather/favourites`, id).pipe(take(1));
  }
}
