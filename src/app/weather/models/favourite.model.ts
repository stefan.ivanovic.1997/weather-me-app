export class Favourite {
  id?: number;
  userId?: number;
  locationKey?: number;
  city?: string;
  country?: string;
  forecast?: Forecast;
}

export class Forecast {
  day?: string;
  date?: string;
  temperature?: { minimum?: number, maximum?: number, unit?: string };
  description?: string;
}

export class Geolocation {
  lat?: number;
  long?: number;
}
