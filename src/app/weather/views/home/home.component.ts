import {Component, OnDestroy, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Observable, of, Subscription} from 'rxjs';
import {map, switchMap, take} from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';
import {WeatherService} from '../../services/weather.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../auth/models/user.model';
import {AuthService} from '../../../auth/services/auth.service';
import {Favourite, Forecast, Geolocation} from '../../models/favourite.model';
import {ENGLISH_LETTERS_REGEX} from '../../../core/constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  cities: any = [];
  selectedCity = new FormControl(null, [Validators.pattern(ENGLISH_LETTERS_REGEX)]);
  filteredOptions: Observable<any[]>;
  user: User;
  private userSub: Subscription;
  private locationSub: Subscription;
  weather: Forecast [] = [];
  mainWeather: Forecast = null;
  city: any = null;
  isFavourite = false;
  favourite: Favourite = null;

  private shouldTriggerChange = true;

  constructor(
    private title: Title,
    private weatherService: WeatherService,
    private authService: AuthService,
    private loader: NgxSpinnerService,
    private toaster: ToastrService
  ) {
    this.title.setTitle('WeatherMe | Home');
  }

  ngOnInit(): void {
    this.userSub = this.authService.user.subscribe(res => this.user = res);
    this.locationSub = this.selectedCity.valueChanges.subscribe(value => {
      if (this.shouldTriggerChange) {
        this.weatherService.getLocations(value).subscribe(res => {
          this.cities = res;
        }, err => this.toaster.error('Error retrieving locations'));
      }
    });
    this.filteredOptions = this.selectedCity.valueChanges
      .pipe(
        map(value => this._filter(value))
      );
    this.setGeolocation();
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
    this.locationSub.unsubscribe();
  }

  private setGeolocation(): void {
    if (navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(response => {
        const geo: Geolocation = {
          lat: response.coords.latitude,
          long: response.coords.longitude
        };
        this.weatherService.getInitialLocation(geo)
          .pipe(
            switchMap(res => {
              this.city = res;
              this.shouldTriggerChange = false;
              if (this.user) {
                return this.weatherService.getLocations(res.LocalizedName);
              }
              return of(null);
            }))
          .subscribe(cities => {
            if (cities) {
              this.cities = cities;
              const arrayValue = this.cities.find(city => city.Key === this.city.Key);
              if (!!arrayValue) {
                this.selectedCity.setValue(arrayValue.LocalizedName + ' - ' + arrayValue.Country.LocalizedName);
                if (this.user) {
                  this.queryLocation();
                }
              }
            }
            this.shouldTriggerChange = true;
          });
      }, () => {
        this.toaster.error('Error retrieving current geolocation');
        this.shouldTriggerChange = true;
      });
    }
  }

  private _filter(value: any): string[] {
    const filterValue = value.toLowerCase();
    return this.cities.filter(option => {
      return option.LocalizedName.toLowerCase().includes(filterValue);
    });
  }

  queryLocation(): void {
    const {city, country} = this.extractInfo();
    const location = this.cities.find(elem => elem.LocalizedName === city && elem.Country.LocalizedName === country);
    if (location) {
      this.city = location;
      const key = +location.Key;
      this.loader.show();
      this.weatherService.getWeather({key, fahrenheit: this.user.fahrenheit})
        .subscribe(res => {
            if (res && res.length > 0) {
              if (this.user.favourites) {
                this.favourite = this.user.favourites.find(elem => +elem.locationKey === key);
                this.isFavourite = !!this.favourite;
              }
              this.weather = res;
              this.mainWeather = this.weather[0];
            }
            this.loader.hide();
          },
          () => {
            this.toaster.error('Error retrieving weather');
            this.loader.hide();
          });
    }
  }

  changeFavouriteStatus(): void {
    if (this.isFavourite && this.favourite) {
      this.weatherService.deleteFavourites(this.favourite.id).subscribe(res => {
        this.authService.reloadUserFromDb(+this.user.id);
        this.changeFavourite();
      }, err => this.toaster.error('Error removing city from favourites'));
    } else {
      const favourite: Favourite = {
        locationKey: +this.city.Key,
        userId: +this.user.id,
        city: this.city.LocalizedName,
        country: this.city.Country.LocalizedName
      };
      this.weatherService.addFavourites(favourite).subscribe(res => {
        this.changeFavourite();
      }, err => this.toaster.error('Error adding city to favourites'));
    }
  }

  private changeFavourite(): void {
    this.authService.reloadUserFromDb(+this.user.id);
    this.isFavourite = !this.isFavourite;
  }

  private extractInfo(): any {
    const data = this.selectedCity.value.split(' - ');
    const city = data[0];
    const country = data[1];
    return {city, country};
  }

}
