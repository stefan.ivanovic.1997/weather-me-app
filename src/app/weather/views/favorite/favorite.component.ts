import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {WeatherService} from '../../services/weather.service';
import {Favourite} from '../../models/favourite.model';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss']
})
export class FavoriteComponent implements OnInit {

  favourites: Favourite[] = [];

  constructor(
    private title: Title,
    private weatherService: WeatherService,
    private toaster: ToastrService
  ) {
    this.title.setTitle('WeatherMe | Favourites');
  }

  ngOnInit(): void {
    this.getFavourites();
  }

  getFavourites(): void {
    this.weatherService.getFavourites().subscribe(
      res => this.favourites = res,
      () => this.toaster.error('Error removing from favourites'));
  }

  removeFavourite(data: Favourite): void {
    this.weatherService.deleteFavourites(data.id).subscribe(res => {
      this.getFavourites();
    }, () => this.toaster.error('Error removing from favourites'));
  }

}
