import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FavoriteComponent} from './views/favorite/favorite.component';
import {HomeComponent} from './views/home/home.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {WeatherService} from './services/weather.service';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'favourite', component: FavoriteComponent}
];

@NgModule({
  declarations: [
    FavoriteComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    WeatherService
  ]
})
export class WeatherModule {
}
