import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './core/views/page-not-found/page-not-found.component';
import {AuthGuardService} from './shared/services/guards/auth.guard.service';
import {RegisteredGuardService} from './shared/services/guards/registered.guard.service';

const routes: Routes = [
  {path: '', redirectTo: 'weather', pathMatch: 'full'},
  {path: 'weather', canActivate: [AuthGuardService], loadChildren: () => import('./weather/weather.module').then(m => m.WeatherModule)},
  {path: 'auth', canActivate: [RegisteredGuardService], loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
